﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OceanicAPI.Models
{
    public class City
    {


        [NotMapped]

        public bool Visited = false;
        [NotMapped]

        public double NodeWeight = 0;

        [NotMapped]

        public int NodeTimeWeight= 0;


        [NotMapped]
        public City? NearestToStart;

        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required]
        public string Name { get; set; }

        public List<CityRoute> CityRoutes { get; set; }
    }
}
