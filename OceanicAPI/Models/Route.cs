﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OceanicAPI.Models
{
    public class Route
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        public List<CityRoute> CityRoutes { get; set; }
    }
}
