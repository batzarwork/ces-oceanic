﻿namespace OceanicAPI.Models.External
{
    public class ExternalCommunicationRouteRequest
    {
        public string StartCity { get; set; }

        public int Weight { get; set; }

        public double[] Dimensions { get; set; } = new double[3];

        public string Type { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;
    }
}
