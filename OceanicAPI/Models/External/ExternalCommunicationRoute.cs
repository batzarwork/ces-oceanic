﻿namespace OceanicAPI.Models.External
{
    public class ExternalCommunicationRoute
    {
        public string DestinationCity { get; set; }

        public double Price { get; set; }

        public int Time { get; set; }
    }
}
