﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OceanicAPI.Models
{
    public class ParcelRoute
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid? CityOneId { get; set; }

        public Guid? CityTwoId { get; set; }

        [ForeignKey("CityOneId")]
        public City? CityOne { get; set; }

        [ForeignKey("CityTwoId")]
        public City? CityTwo { get; set; }

        [Required]
        public double Price { get; set; }

        public string GetPrice()
        {
            return $"{Price}$";
        }

        [Required]
        public int EstimatedTimeArrival { get; set; }

        [Required]
        public Guid ParcelId { get; set; }

        public string TransportationType { get; set; }
    }
}
