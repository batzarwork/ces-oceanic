﻿using System.ComponentModel.DataAnnotations;

namespace OceanicAPI.Models
{
    public class CityRoute
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid CityId { get; set; }

        public City City { get; set; }

        public Guid RouteId { get; set; }

        public Route Route { get; set; }
    }
}
