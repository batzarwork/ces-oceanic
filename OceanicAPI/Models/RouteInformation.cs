﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OceanicAPI.Models
{
    public class RouteInformation
    {
        public int Time { get; set; }

        public double Price { get; set; }


        public List<City> Cities { get; set; }
    }
}
