﻿using System.ComponentModel.DataAnnotations;

namespace OceanicAPI.Models
{
    public class Parcel
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required]
        public int Weight { get; set; }

        [Required]
        public double[] Dimensions { get; set; } = new double[3];

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string Type { get; set; }

        [Required]
        public DateTime EstimatedTimeArrival { get; set; }

        [Required]
        public double Price { get; set; }

        public List<ParcelRoute> ParcelRoutes { get; set; }
    }
}
