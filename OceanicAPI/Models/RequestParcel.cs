﻿using System.ComponentModel.DataAnnotations;

namespace OceanicAPI.Models
{
    public class RequestParcel
    {
        public string Option { get; set; }
        public int Weight { get; set; }

        public double[] Dimentions { get; set; } = new double[3];

        public string Type { get; set; }

        public string StartCity { get; set; }

        public string EndCity { get; set; }

        public DateTime Date { get; set; }

    }
}
