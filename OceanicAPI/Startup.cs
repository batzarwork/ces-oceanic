﻿using Common.Repositories.Implementations;
using Common.Repositories.Interfaces;
using Common.ServiceConfiguration;
using Microsoft.EntityFrameworkCore;
using Oceanic.Context;
using OceanicAPI.Models;
using OceanicAPI.Repositories.Implementations;
using OceanicAPI.Repositories.Interfaces;
using OceanicAPI.Services.Implementations;
using OceanicAPI.Services.Interfaces;

namespace OceanicAPI
{
    /// <summary>
    /// Startup configuration.
    /// </summary>
    public class Startup
    {
        private const string OpenApiVersion = "v1";

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">Injected <see cref="IConfiguration"/>.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Instance of <see cref="IServiceCollection"/> to configure.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureServices();
            services.AddConvertersAndPopulators();

            var connectionType = Configuration.GetConnectionString("Type");
            services.AddDbContext<OceanicApiContext>(dbContextOptions => dbContextOptions.UseSqlServer(Configuration.GetConnectionString("Default")), ServiceLifetime.Transient);

            AddServices(services);
            AddRepositories(services);
            services.AddHttpContextAccessor();
            services.AddHealthChecks();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Instance of <see cref="IApplicationBuilder"/> to configure.</param>
        /// <param name="env">Environment for configuration. See <see cref="IWebHostEnvironment"/>.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.ConfigureBuilder(OpenApiVersion, env, true);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");
            });
        }

        private static void AddRepositories(IServiceCollection services)
        {
            services.AddScoped<IDestinationRepository, DestinationRepository>();
            services.AddScoped<ICityRouteRepository, CityRouteRepository>();
            services.AddScoped<IRouteRepository, RouteRepository>();
            services.AddScoped<IParcelRepository, ParcelRepository>();
            services.AddScoped<IParcelRouteRepository, ParcelRouteRepository>();
        }

        private static void AddServices(IServiceCollection services)
        {
            services.AddScoped<IDestinationService, DestinationService>();
            services.AddScoped<IExternalCommunicationService, ExternalCommunicationService>();
            services.AddScoped<ICityRouteService, CityRouteService>();
            services.AddScoped<IComputingService, ComputingService>();
            services.AddScoped<IRouteService, RouteService>();
            services.AddScoped<IParcelService, ParcelService>();
        }
    }
}
