﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using OceanicAPI.Models;

namespace Oceanic.Context
{
    /// <summary>
    /// Context for Oceanic api.
    /// </summary>
    public class OceanicApiContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OceanicApiContext"/> class.
        /// </summary>
        /// <param name="options">option parameter.</param>
        public OceanicApiContext(DbContextOptions<OceanicApiContext> options)
            : base(options)
        {
        }

        public DbSet<City> Cities { get; set; } = null!;

        public DbSet<CityRoute> CityRoutes { get; set; } = null!;

        public DbSet<Parcel> Parcels { get; set; } = null!;

        public DbSet<ParcelRoute> ParcelRoutes { get; set; } = null!;

        public DbSet<OceanicAPI.Models.Route> Routes { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new ParcelEntityTypeConfiguration().Configure(modelBuilder.Entity<Parcel>());
            modelBuilder.Entity<CityRoute>()
            .HasKey(bc => new { bc.CityId, bc.RouteId });
            modelBuilder.Entity<CityRoute>()
            .HasOne(s => s.City)
            .WithMany(g => g.CityRoutes)
            .HasForeignKey(s => s.CityId);
            modelBuilder.Entity<CityRoute>()
            .HasOne(s => s.Route)
            .WithMany(g => g.CityRoutes)
            .HasForeignKey(s => s.RouteId);

            modelBuilder.Entity<City>().HasData(
                new List<City>
                {
                    new City
                    {
                        Id = Guid.Parse("e993bd17-4c79-4af4-9e85-023b72f57a98"),
                        Name = "Suakin",
                    },
                    new City
                    {
                        Id = Guid.Parse("2abfcbd3-f0ae-4f4c-bf2d-5be8119b6344"),
                        Name = "Cairo",
                    },
                    new City
                    {
                        Id = Guid.Parse("b6726a80-4576-48a6-95a7-bea17f28b160"),
                        Name = "Darfur",
                    },
                    new City
                    {
                        Id = Guid.Parse("325972ae-9157-4043-af46-0ae99301d613"),
                        Name = "Tripoli",
                    },
                    new City
                    {
                        Id = Guid.Parse("178e3ba8-e3e3-401a-91a7-306a7ab3f637"),
                        Name = "Kabalo",
                    },
                    new City
                    {
                        Id = Guid.Parse("5ea45eff-496a-4ee9-9ce3-59bab2d814aa"),
                        Name = "Tanger",
                    },
                    new City
                    {
                        Id = Guid.Parse("04b1949c-75a1-4b82-838b-0903a3814379"),
                        Name = "Marrakesh",
                    },
                    new City
                    {
                        Id = Guid.Parse("45dbf238-0976-47ee-a327-c5187c7e042b"),
                        Name = "Guldkysten",
                    },
                    new City
                    {
                        Id = Guid.Parse("74f4dec7-535c-4ed6-87e3-a82b68335275"),
                        Name = "Sierra Leone",
                    },
                    new City
                    {
                        Id = Guid.Parse("ffb51563-61c4-499c-b93e-1b688145cb90"),
                        Name = "St. Helena",
                    },
                    new City
                    {
                        Id = Guid.Parse("bc04251a-50b5-4947-b40d-2ee5fa246a42"),
                        Name = "Luanda",
                    },
                    new City
                    {
                        Id = Guid.Parse("2a1d4ca8-830e-40ae-840c-bb20f894cbf7"),
                        Name = "Hvalbugten",
                    },
                    new City
                    {
                        Id = Guid.Parse("325f4b31-37b8-4912-ba23-39fe59709487"),
                        Name = "Kapstaden",
                    },
                    new City
                    {
                        Id = Guid.Parse("8a0ad3e0-ddf8-4d7d-8927-ec9ac44875aa"),
                        Name = "Dragebjerget",
                    },
                    new City
                    {
                        Id = Guid.Parse("9d440382-d7ea-45a3-b45c-484569138eb4"),
                        Name = "Kap St. Marie",
                    },
                    new City
                    {
                        Id = Guid.Parse("fa8f76a2-33aa-46c0-9f88-df18664a410f"),
                        Name = "Amatave",
                    },
                    new City
                    {
                        Id = Guid.Parse("a0feddd9-69b2-4bff-b87e-8d98ab7ddf19"),
                        Name = "Victoria-søen",
                    },
                    new City
                    {
                        Id = Guid.Parse("17e7d18e-3a3b-44fb-987d-c265876e2623"),
                        Name = "Kap Guardafui",
                    },
                });
            _ = modelBuilder.Entity<CityRoute>().HasData(
                new List<CityRoute>
                {
                    // Suakin
                    new CityRoute
                    {
                        CityId = Guid.Parse("e993bd17-4c79-4af4-9e85-023b72f57a98"),
                        RouteId = Guid.Parse("0cef40ee-f3ba-4496-b6de-b74ae4d962fb"), // Cairo
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("e993bd17-4c79-4af4-9e85-023b72f57a98"),
                        RouteId = Guid.Parse("6fd2f427-8521-43a2-8990-75caea1f28c0"), // Dafur
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("e993bd17-4c79-4af4-9e85-023b72f57a98"),
                        RouteId = Guid.Parse("2a765283-71e3-40a5-b775-607b6a18d4b1"), // Victoria-Søen
                    },

                    // Cairo
                    new CityRoute
                    {
                        CityId = Guid.Parse("2abfcbd3-f0ae-4f4c-bf2d-5be8119b6344"),
                        RouteId = Guid.Parse("0cef40ee-f3ba-4496-b6de-b74ae4d962fb"), // Suakin
                    },

                    // Dafur
                    new CityRoute
                    {
                        CityId = Guid.Parse("b6726a80-4576-48a6-95a7-bea17f28b160"),
                        RouteId = Guid.Parse("6fd2f427-8521-43a2-8990-75caea1f28c0"), // Suakin
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("b6726a80-4576-48a6-95a7-bea17f28b160"),
                        RouteId = Guid.Parse("085d026b-9194-4275-b6af-217e8d187d8d"), // Tripoli
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("b6726a80-4576-48a6-95a7-bea17f28b160"),
                        RouteId = Guid.Parse("8548d2db-586a-4ed4-86ce-75d53f771fbd"), // Kabalo
                    },

                    // Victoria-søen
                    new CityRoute
                    {
                        CityId = Guid.Parse("a0feddd9-69b2-4bff-b87e-8d98ab7ddf19"),
                        RouteId = Guid.Parse("2a765283-71e3-40a5-b775-607b6a18d4b1"), // Suakin
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("a0feddd9-69b2-4bff-b87e-8d98ab7ddf19"),
                        RouteId = Guid.Parse("88a575b0-2f64-46c7-8f8f-b6ef8e2d506d"), // Dragebjerget
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("a0feddd9-69b2-4bff-b87e-8d98ab7ddf19"),
                        RouteId = Guid.Parse("c565afa1-4526-47fb-b475-3dc36cb2a32b"), // Kap Guardafui
                    },

                    // Kabalo
                    new CityRoute
                    {
                        CityId = Guid.Parse("178e3ba8-e3e3-401a-91a7-306a7ab3f637"),
                        RouteId = Guid.Parse("8548d2db-586a-4ed4-86ce-75d53f771fbd"), // Dafur
                    },
                    //new cityroute
                    //{
                    //    cityid = guid.parse("178e3ba8-e3e3-401a-91a7-306a7ab3f637"),
                    //    routeid = guid.parse("10aa4d05-34e5-471f-8fb3-bc4ce9acbbdb"), // kapstaden
                    //},

                    // Tripoli
                    new CityRoute
                    {
                        CityId = Guid.Parse("325972ae-9157-4043-af46-0ae99301d613"),
                        RouteId = Guid.Parse("085d026b-9194-4275-b6af-217e8d187d8d"), // Dafur
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("325972ae-9157-4043-af46-0ae99301d613"),
                        RouteId = Guid.Parse("a89e295e-1010-426c-aa9a-c08f8bf0c621"), // Tanger
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("325972ae-9157-4043-af46-0ae99301d613"),
                        RouteId = Guid.Parse("d33d6643-f88b-49b1-90dd-ef42b81cc630"), // Guldkysten
                    },

                    // Tanger
                    new CityRoute
                    {
                        CityId = Guid.Parse("5ea45eff-496a-4ee9-9ce3-59bab2d814aa"),
                        RouteId = Guid.Parse("a89e295e-1010-426c-aa9a-c08f8bf0c621"), // Tripoli
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("5ea45eff-496a-4ee9-9ce3-59bab2d814aa"),
                        RouteId = Guid.Parse("fc9276f2-c221-4abd-b2e4-e8ea782e95d4"), // Marrakesh
                    },

                    // Marrakesh
                    new CityRoute
                    {
                        CityId = Guid.Parse("04b1949c-75a1-4b82-838b-0903a3814379"),
                        RouteId = Guid.Parse("fc9276f2-c221-4abd-b2e4-e8ea782e95d4"), // Tanger
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("04b1949c-75a1-4b82-838b-0903a3814379"),
                        RouteId = Guid.Parse("0b71b141-57c1-440e-a8ad-8323992c1ea8"), // Sierra Leone
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("04b1949c-75a1-4b82-838b-0903a3814379"),
                        RouteId = Guid.Parse("fb2c8d13-91e3-44df-8aeb-8021695b3ee3"), // Guldkysten
                    },

                    // Guldkysten
                    new CityRoute
                    {
                        CityId = Guid.Parse("45dbf238-0976-47ee-a327-c5187c7e042b"),
                        RouteId = Guid.Parse("d33d6643-f88b-49b1-90dd-ef42b81cc630"), // Tripoli
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("45dbf238-0976-47ee-a327-c5187c7e042b"),
                        RouteId = Guid.Parse("fb2c8d13-91e3-44df-8aeb-8021695b3ee3"), // Marrakesh
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("45dbf238-0976-47ee-a327-c5187c7e042b"),
                        RouteId = Guid.Parse("152c42ca-45e8-4203-af0a-d5f2712b9637"), // Luanda
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("45dbf238-0976-47ee-a327-c5187c7e042b"),
                        RouteId = Guid.Parse("0bfbdfe7-a138-4d09-8235-e42ccaa77d11"), // Hvalbugten
                    },

                     // Sierra Leone
                    new CityRoute
                    {
                        CityId = Guid.Parse("74f4dec7-535c-4ed6-87e3-a82b68335275"),
                        RouteId = Guid.Parse("0b71b141-57c1-440e-a8ad-8323992c1ea8"), // Marrakesh
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("74f4dec7-535c-4ed6-87e3-a82b68335275"),
                        RouteId = Guid.Parse("ec417fc0-135a-4dc4-a723-21545e00bba0"), // St. Helena
                    },

                     // St Helena
                    new CityRoute
                    {
                        CityId = Guid.Parse("ffb51563-61c4-499c-b93e-1b688145cb90"),
                        RouteId = Guid.Parse("ec417fc0-135a-4dc4-a723-21545e00bba0"), // Sierra Leone
                    },
                    //new cityroute
                    //{
                    //    cityid = guid.parse("ffb51563-61c4-499c-b93e-1b688145cb90"),
                    //    routeid = guid.parse("10d0fcd7-10e8-4c06-a827-84b668e5f0e3"), // kapstaden
                    //},

                    // Luanda
                    new CityRoute
                    {
                        CityId = Guid.Parse("bc04251a-50b5-4947-b40d-2ee5fa246a42"),
                        RouteId = Guid.Parse("152c42ca-45e8-4203-af0a-d5f2712b9637"), // Guldkysten
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("bc04251a-50b5-4947-b40d-2ee5fa246a42"),
                        RouteId = Guid.Parse("d46db2fd-3ed5-4703-929a-86332506b775"), // Hvalbugten
                    },

                    // Hvalbugten
                    new CityRoute
                    {
                        CityId = Guid.Parse("2a1d4ca8-830e-40ae-840c-bb20f894cbf7"),
                        RouteId = Guid.Parse("0bfbdfe7-a138-4d09-8235-e42ccaa77d11"), // Guldkysten
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("2a1d4ca8-830e-40ae-840c-bb20f894cbf7"),
                        RouteId = Guid.Parse("d46db2fd-3ed5-4703-929a-86332506b775"), // Luanda
                    },
                    //new CityRoute
                    //{
                    //    CityId = Guid.Parse("2a1d4ca8-830e-40ae-840c-bb20f894cbf7"),
                    //    RouteId = Guid.Parse("b6be396d-3ceb-4657-9ed7-5da33e378d89"), // Kapstaden
                    //},

                    // Kapstaden   -- Terroattack 
                    //new CityRoute
                    //{
                    //    CityId = Guid.Parse("325f4b31-37b8-4912-ba23-39fe59709487"),
                    //    RouteId = Guid.Parse("10aa4d05-34e5-471f-8fb3-bc4ce9acbbdb"), // Kabalo
                    //},
                    //new CityRoute
                    //{
                    //    CityId = Guid.Parse("325f4b31-37b8-4912-ba23-39fe59709487"),
                    //    RouteId = Guid.Parse("10d0fcd7-10e8-4c06-a827-84b668e5f0e3"), // St Helena
                    //},
                    //new CityRoute
                    //{
                    //    CityId = Guid.Parse("325f4b31-37b8-4912-ba23-39fe59709487"),
                    //    RouteId = Guid.Parse("b6be396d-3ceb-4657-9ed7-5da33e378d89"), // Hvalbugten
                    //},
                    //new CityRoute
                    //{
                    //    CityId = Guid.Parse("325f4b31-37b8-4912-ba23-39fe59709487"),
                    //    RouteId = Guid.Parse("b8c08d0f-544d-4848-a7ac-f66fd8a11894"), // Dragebjerget
                    //},
                    //new CityRoute
                    //{
                    //    CityId = Guid.Parse("325f4b31-37b8-4912-ba23-39fe59709487"),
                    //    RouteId = Guid.Parse("c0ecfce3-37a2-4795-a4e4-84b28199e2fe"), // Amatave
                    //},
                    //new CityRoute
                    //{
                    //    CityId = Guid.Parse("325f4b31-37b8-4912-ba23-39fe59709487"),
                    //    RouteId = Guid.Parse("1863ddce-2a4a-4020-8df6-b585d73ef12e"), // Kap St. Marie
                    //},

                    // Dragebjerget
                    new CityRoute
                    {
                        CityId = Guid.Parse("8a0ad3e0-ddf8-4d7d-8927-ec9ac44875aa"),
                        RouteId = Guid.Parse("88a575b0-2f64-46c7-8f8f-b6ef8e2d506d"), // Victoria-søen
                    },
                    //new CityRoute
                    //{
                    //    CityId = Guid.Parse("8a0ad3e0-ddf8-4d7d-8927-ec9ac44875aa"),
                    //    RouteId = Guid.Parse("b8c08d0f-544d-4848-a7ac-f66fd8a11894"), // Kapstaden
                    //},

                    // Amatave
                    new CityRoute
                    {
                        CityId = Guid.Parse("fa8f76a2-33aa-46c0-9f88-df18664a410f"),
                        RouteId = Guid.Parse("0df525c3-4043-4034-8ad9-044fcdfdc447"), // Kap Guardafui
                    },
                    //new CityRoute
                    //{
                    //    CityId = Guid.Parse("fa8f76a2-33aa-46c0-9f88-df18664a410f"),
                    //    RouteId = Guid.Parse("c0ecfce3-37a2-4795-a4e4-84b28199e2fe"), // Kapstaden
                    //},

                    // Kap Guardafui
                    new CityRoute
                    {
                        CityId = Guid.Parse("17e7d18e-3a3b-44fb-987d-c265876e2623"),
                        RouteId = Guid.Parse("0df525c3-4043-4034-8ad9-044fcdfdc447"), // Amatave
                    },
                    new CityRoute
                    {
                        CityId = Guid.Parse("17e7d18e-3a3b-44fb-987d-c265876e2623"),
                        RouteId = Guid.Parse("c565afa1-4526-47fb-b475-3dc36cb2a32b"), // Victoria-søen
                    },

                });
            modelBuilder.Entity<OceanicAPI.Models.Route>().HasData(
                new List<OceanicAPI.Models.Route>
               {
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("0cef40ee-f3ba-4496-b6de-b74ae4d962fb"), // Suakin - Cairo
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("6fd2f427-8521-43a2-8990-75caea1f28c0"), // Suakin - Dafur
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("2a765283-71e3-40a5-b775-607b6a18d4b1"), // Suakin - Victoria søen
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("8548d2db-586a-4ed4-86ce-75d53f771fbd"), // Kabalo - Dafur
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("085d026b-9194-4275-b6af-217e8d187d8d"), // Dafur - Tripoli
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("a89e295e-1010-426c-aa9a-c08f8bf0c621"), // Tripoli - Tanger
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("fc9276f2-c221-4abd-b2e4-e8ea782e95d4"), // Tanger - Marrakesh
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("d33d6643-f88b-49b1-90dd-ef42b81cc630"), // Tripoli - Guldkysten
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("fb2c8d13-91e3-44df-8aeb-8021695b3ee3"), // Marrakesh - Guldkysten
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("0b71b141-57c1-440e-a8ad-8323992c1ea8"), // Marrakesh - Sierra Leone
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("ec417fc0-135a-4dc4-a723-21545e00bba0"), // St Helena - Sierra Leone
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("152c42ca-45e8-4203-af0a-d5f2712b9637"), // Guldkysten - Luanda
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("0bfbdfe7-a138-4d09-8235-e42ccaa77d11"), // Guldkysten - Hvalbugten
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("10aa4d05-34e5-471f-8fb3-bc4ce9acbbdb"), // Kapstaden - Kabalo
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("10d0fcd7-10e8-4c06-a827-84b668e5f0e3"), // Kapstaden - St Helena
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("b6be396d-3ceb-4657-9ed7-5da33e378d89"), // Kapstaden - Hvalbugten
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("88a575b0-2f64-46c7-8f8f-b6ef8e2d506d"), // Victoria søen - Dragebjerget
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("0df525c3-4043-4034-8ad9-044fcdfdc447"), // Kap Guardafui - Amatave
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("c565afa1-4526-47fb-b475-3dc36cb2a32b"), // Kap Guardafui - Victoria søen
                    },
                    new OceanicAPI.Models.Route
                    {
                        Id = Guid.Parse("d46db2fd-3ed5-4703-929a-86332506b775"), // Hvalbugten - Luanda
                    },
               });
            modelBuilder.Entity<Parcel>().HasData(
                new List<Parcel>
                {
                    new Parcel
                    {
                        Id = Guid.Parse("7c7611fb-4c29-4244-a3b8-c8d585cb084d"),
                        Dimensions = new double[] { 1, 2.3, 0.4 },
                        Weight = 1000,
                        Price = 100,
                        StartDate = new DateTime(2022, 08, 25),
                        EstimatedTimeArrival = new DateTime(2022, 08, 26, 12, 10, 10),
                        State = "complited",
                        Type = "LiveAnimal",
                    },
                });
        }

        public class ParcelEntityTypeConfiguration : IEntityTypeConfiguration<Parcel>
        {
            public void Configure(EntityTypeBuilder<Parcel> builder)
            {
                builder
                 .Property(e => e.Dimensions).HasConversion(
                    v => JsonConvert.SerializeObject(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }),
                    v => JsonConvert.DeserializeObject<double[]>(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
            }
        }

        /// <summary>
        /// Get Dbset.
        /// </summary>
        /// <typeparam name="T">Entity model.</typeparam>
        /// <returns>Dbset.</returns>
        public DbSet<T> GetSet<T>()
            where T : class
        {
            return Set<T>();
        }
    }
}
