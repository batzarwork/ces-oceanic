﻿using OceanicAPI.Models;
using Route = OceanicAPI.Models.Route;

namespace OceanicAPI.Services.Interfaces
{
    public interface IRouteService
    {
        public IEnumerable<Route> GetAllRoutes();

    }
}
