﻿using OceanicAPI.Models.External;

namespace OceanicAPI.Services.Interfaces
{
    /// <summary>
    /// Service for handling communication with external system.
    /// </summary>
    public interface IExternalCommunicationService
    {
        public List<ExternalCommunicationRoute> GetInternalRoutes(ExternalCommunicationRouteRequest request);

        public Task<List<ExternalCommunicationRoute>> GetExternalRoutes(ExternalCommunicationRouteRequest request,string url);
    }
}
