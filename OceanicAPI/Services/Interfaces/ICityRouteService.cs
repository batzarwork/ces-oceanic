﻿using OceanicAPI.Models;

namespace OceanicAPI.Services.Interfaces
{
    public interface ICityRouteService
    {
        public IEnumerable<CityRoute> GetAllCityRoutes();

    }
}
