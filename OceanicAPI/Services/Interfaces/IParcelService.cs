﻿using OceanicAPI.Models;

namespace OceanicAPI.Services.Interfaces
{
    public interface IParcelService
    {
        public IEnumerable<Parcel> GetAll();

        public Parcel Add(Parcel parcel);
    }
}
