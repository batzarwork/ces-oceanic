﻿using OceanicAPI.Models;

namespace OceanicAPI.Services.Interfaces
{
    public interface IDestinationService
    {
        public IEnumerable<City> GetAllCities();
        public City GetCityByName(string name);
    }
}
