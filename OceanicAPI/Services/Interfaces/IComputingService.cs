﻿using OceanicAPI.Models;

namespace OceanicAPI.Services.Interfaces
{
    public interface IComputingService
    {
        public RouteInformation Computing(RequestParcel requestParcel);

    }
}
