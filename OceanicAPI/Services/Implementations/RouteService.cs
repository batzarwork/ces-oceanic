﻿using OceanicAPI.Repositories.Interfaces;
using OceanicAPI.Services.Interfaces;

namespace OceanicAPI.Services.Implementations
{
    /// <inheritdoc/>
    public class RouteService : IRouteService
    {
        public RouteService(
            IRouteRepository routeServiceRepository)
        {
            RouteServiceRepository = routeServiceRepository;
        }

        public IRouteRepository RouteServiceRepository { get; }

        public IEnumerable<Models.Route> GetAllRoutes()
        {
            return RouteServiceRepository.Read().ToList();
        }
    }
}
