

using Elastic.Apm.Api;
using NuGet.Versioning;
using OceanicAPI.Models;
using OceanicAPI.Models.External;
using OceanicAPI.Services.Interfaces;

namespace OceanicAPI.Services.Implementations
{
    public class ComputingService : IComputingService
    {

        public ComputingService(
            IDestinationService destinationService,
            IExternalCommunicationService externalCommunicationService,
            ICityRouteService cityRouteService,
            IRouteService routeService)
        {
            DestinationService = destinationService;
            ExternalCommunicationService = externalCommunicationService;
            CityRouteService = cityRouteService;
            RouteService = routeService;
        }

        public IDestinationService DestinationService { get; }
        public IExternalCommunicationService ExternalCommunicationService { get; }
        public ICityRouteService CityRouteService { get; }
        public IRouteService RouteService { get; }

        public RouteInformation Computing(RequestParcel requestParcel)
        {
            DijkstraSearch(requestParcel);
            var shortestPath = new List<City>();
            City endCity = DestinationService.GetCityByName(requestParcel.EndCity);
            shortestPath.Add(endCity);
            BuildShortestPath(shortestPath, endCity);
            shortestPath.Reverse();

            return new RouteInformation {Cities = shortestPath, Price= endCity.NodeWeight, Time= endCity.NodeTimeWeight};
        }

        private void BuildShortestPath(List<City> list, City node)
        {
            if (node.NearestToStart == null)
                return;
            list.Add(node.NearestToStart);
            BuildShortestPath(list, node.NearestToStart);
        }

        private City DijkstraSearch(RequestParcel requestParcel)
        {
            var allCities = DestinationService.GetAllCities();
            City startCity = allCities.FirstOrDefault(s => s.Name == requestParcel.StartCity);

            startCity.NodeWeight = 0;
            startCity.NodeTimeWeight = 0;
            var prioQueue = new List<City>();
            prioQueue.Add(startCity);
            switch (requestParcel.Option)
            {
                case "Price":
                    do
                    {
                        prioQueue = prioQueue.DistinctBy(s => s.Name).OrderBy(x => x.NodeWeight).ToList();
                        var workingCity = prioQueue.First();
                        prioQueue.Remove(workingCity);
                        var externalCommunicationRoute = new ExternalCommunicationRouteRequest { StartCity = workingCity.Name, Date = requestParcel.Date, Dimensions = requestParcel.Dimentions, Type = requestParcel.Type, Weight = requestParcel.Weight };
                        var externalCommunicationRoutes = ExternalCommunicationService
                            .GetInternalRoutes(externalCommunicationRoute)
                            .Where(s => s.DestinationCity != (workingCity.NearestToStart is not null ? workingCity.NearestToStart.Name : string.Empty))
                            .OrderBy(s => s.Price);

                        foreach (var cnn in externalCommunicationRoutes)
                        {
                            var childNode = allCities.FirstOrDefault(s => s.Name == cnn.DestinationCity);

                            if (childNode.Visited)
                                continue;
                            if ((workingCity.NodeWeight + cnn.Price) > childNode.NodeWeight)
                            {
                                allCities.ToList().Remove(childNode);
                                childNode.NodeWeight = workingCity.NodeWeight + cnn.Price;
                                childNode.NodeTimeWeight = workingCity.NodeTimeWeight + cnn.Time;
                                childNode.NearestToStart = workingCity;
                                prioQueue.Add(childNode);
                                childNode.Visited = true;
                                allCities.ToList().Add(childNode);
                            }
                        }
                        if (workingCity.Name == requestParcel.EndCity)
                            return workingCity;
                    } while (prioQueue.Any());
                    break;
                case "Time":
                    do
                    {
                        prioQueue = prioQueue.DistinctBy(s => s.Name).OrderBy(x => x.NodeTimeWeight).ToList();
                        var workingCity = prioQueue.First();
                        prioQueue.Remove(workingCity);
                        var externalCommunicationRoute = new ExternalCommunicationRouteRequest { StartCity = workingCity.Name, Date = requestParcel.Date, Dimensions = requestParcel.Dimentions, Type = requestParcel.Type, Weight = requestParcel.Weight };
                        var externalCommunicationRoutes = ExternalCommunicationService
                            .GetInternalRoutes(externalCommunicationRoute)
                            .Where(s => s.DestinationCity != (workingCity.NearestToStart is not null ? workingCity.NearestToStart.Name : string.Empty))
                            .OrderBy(s => s.Time);

                        foreach (var cnn in externalCommunicationRoutes)
                        {
                            var childNode = allCities.FirstOrDefault(s => s.Name == cnn.DestinationCity);

                            if (childNode.Visited)
                                continue;
                            if ((workingCity.NodeTimeWeight + cnn.Time) > childNode.NodeTimeWeight)
                            {
                                allCities.ToList().Remove(childNode);
                                childNode.NodeWeight = workingCity.NodeWeight + cnn.Price;
                                childNode.NodeTimeWeight = workingCity.NodeTimeWeight + cnn.Time;
                                childNode.NearestToStart = workingCity;
                                prioQueue.Add(childNode);
                                childNode.Visited = true;
                                allCities.ToList().Add(childNode);
                            }
                        }
                        if (workingCity.Name == requestParcel.EndCity)
                            return workingCity;
                    } while (prioQueue.Any());
                    break;
                default:
                    do
                    {
                        prioQueue = prioQueue.DistinctBy(s => s.Name).OrderBy(x => x.NodeTimeWeight).ToList();
                        var workingCity = prioQueue.First();
                        prioQueue.Remove(workingCity);
                        var externalCommunicationRoute = new ExternalCommunicationRouteRequest { StartCity = workingCity.Name, Date = requestParcel.Date, Dimensions = requestParcel.Dimentions, Type = requestParcel.Type, Weight = requestParcel.Weight };
                        var externalCommunicationRoutes = ExternalCommunicationService
                            .GetInternalRoutes(externalCommunicationRoute)
                            .Where(s => s.DestinationCity != (workingCity.NearestToStart is not null ? workingCity.NearestToStart.Name : string.Empty))
                            .OrderBy(s => s.Time);

                        foreach (var cnn in externalCommunicationRoutes)
                        {
                            var childNode = allCities.FirstOrDefault(s => s.Name == cnn.DestinationCity);

                            if (childNode.Visited)
                                continue;
                            if ((workingCity.NodeTimeWeight + cnn.Time) > childNode.NodeTimeWeight)
                            {
                                allCities.ToList().Remove(childNode);
                                childNode.NodeWeight = workingCity.NodeWeight + cnn.Price;
                                childNode.NodeTimeWeight = workingCity.NodeTimeWeight + cnn.Time;
                                childNode.NearestToStart = workingCity;
                                prioQueue.Add(childNode);
                                childNode.Visited = true;
                                allCities.ToList().Add(childNode);
                            }
                        }
                        if (workingCity.Name == requestParcel.EndCity)
                            return workingCity;
                    } while (prioQueue.Any());
                    break;
                    break;
            }

            return new City();
        }

        //public async Task<List<OceanicAPI.Models.Route>> GetRoutsFromCity(String StartCity)
        //{
        //    var city = (await DestinationService.GetAllCities())
        //        .FirstOrDefault(x => x.Name == StartCity);
        //    var cityRoutes = (await CityRouteService.GetAllCityRoutes())
        //        .Where(s => s.CityId == city.Id);
        //    var routeIds = cityRoutes.Select(s => s.RouteId);
        //    var routes = (await CityRouteService.GetAllCityRoutes())
        //        .Where(s => routeIds.Contains(s.RouteId));
        //    var externalCommunicationRoutes = new List<ExternalCommunicationRoute>();
        //    List<City> cities;
        //    foreach (var item in
        //        routes.Where(w => w.CityId != city.Id))
        //    {
        //        cities.Add(item.City);
        //    }
        //}
    }
}