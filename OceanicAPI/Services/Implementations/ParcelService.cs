﻿using Common.Repositories.Implementations;
using Common.Repositories.Interfaces;
using OceanicAPI.Models;
using OceanicAPI.Models.External;
using OceanicAPI.Repositories.Interfaces;
using OceanicAPI.Services.Interfaces;

namespace OceanicAPI.Services.Implementations
{
    /// <inheritdoc/>
    public class ParcelService : IParcelService
    {
        public ParcelService(
            IParcelRepository parcelRepository,
            IParcelRouteRepository parcelRouteRepository)
        {
            ParcelRepository = parcelRepository;
            ParcelRouteRepository = parcelRouteRepository;
        }

        public IParcelRepository ParcelRepository { get; }
        public IParcelRouteRepository ParcelRouteRepository { get; }

        public Parcel Add(Parcel parcel)
        {
            ParcelRouteRepository.Create(parcel.ParcelRoutes);
            return ParcelRepository.Create(parcel);
        }

        public IEnumerable<Parcel> GetAll()
        {
            var parcels = ParcelRepository.Read().ToList();
            foreach (var parcel in parcels)
            {
                parcel.ParcelRoutes = ParcelRouteRepository.Read().Where(s => s.ParcelId == parcel.Id).ToList();
            }

            return ParcelRepository.Read().ToList();
        }
    }
}
