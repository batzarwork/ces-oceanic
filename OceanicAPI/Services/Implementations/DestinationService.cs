﻿using Common.Repositories.Implementations;
using Common.Repositories.Interfaces;
using OceanicAPI.Models;
using OceanicAPI.Models.External;
using OceanicAPI.Repositories.Interfaces;
using OceanicAPI.Services.Interfaces;

namespace OceanicAPI.Services.Implementations
{
    /// <inheritdoc/>
    public class DestinationService : IDestinationService
    {
        public DestinationService(
            IDestinationRepository destinationRepository)
        {
            DestinationRepository = destinationRepository;
        }

        private IDestinationRepository DestinationRepository { get; }

        public IEnumerable<City> GetAllCities()
        {
            return DestinationRepository.Read().ToList();
        }

        public City GetCityByName(string name)
        {
            return DestinationRepository.Read().FirstOrDefault(s => s.Name == name);
        }
    }
}
