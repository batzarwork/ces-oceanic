﻿using Common.Repositories.Implementations;
using Common.Repositories.Interfaces;
using Flurl.Http;
using OceanicAPI.Models;
using OceanicAPI.Models.External;
using OceanicAPI.Services.Interfaces;
using System.Text.Json;

namespace OceanicAPI.Services.Implementations
{
    /// <inheritdoc/>
    public class ExternalCommunicationService : IExternalCommunicationService
    {
        public ExternalCommunicationService(
            IDestinationService destinationService,
            ICityRouteService cityRouteService,
            IRouteService routeService)
        {
            DestinationService = destinationService;
            CityRouteService = cityRouteService;
            RouteService = routeService;
        }

        private IDestinationService DestinationService { get; }
        private ICityRouteService CityRouteService { get; }
        private IRouteService RouteService { get; }

        public async Task<List<ExternalCommunicationRoute>> GetExternalRoutes(ExternalCommunicationRouteRequest request,string url)
        {
            var responseString = await url
                  .PostUrlEncodedAsync(request)
                  .ReceiveString();
            return JsonSerializer.Deserialize<List<ExternalCommunicationRoute>>(responseString);
        }

        public List<ExternalCommunicationRoute> GetInternalRoutes(ExternalCommunicationRouteRequest request)
        {
            var city = DestinationService.GetAllCities()
                .FirstOrDefault(x => x.Name == request.StartCity);
            var cityRoutes = CityRouteService.GetAllCityRoutes()
                .Where(s => s.CityId == city.Id);
            var routeIds = cityRoutes.Select(s => s.RouteId);
            var routes = CityRouteService.GetAllCityRoutes()
                .Where(s => routeIds.Contains(s.RouteId));
            var externalCommunicationRoutes = new List<ExternalCommunicationRoute>();
            foreach (var item in
                routes.Where(w => w.CityId != city.Id))
            {
                var destinationCity = DestinationService.GetAllCities().FirstOrDefault(x => x.Id == item.CityId);
                var externalCommunicationRoute = new ExternalCommunicationRoute
                {
                    DestinationCity = destinationCity.Name,
                    Price = GetPrice(request),
                    Time = (8 * 60) + 15
                };
                externalCommunicationRoutes.Add(externalCommunicationRoute);
            }
            return externalCommunicationRoutes;
        }
        public double GetPrice(ExternalCommunicationRouteRequest request)
        {
            double price = 40;

            price = ModifyPriceFromWeight(request, price);
            price = ModifyPriceFromdimentions(request, price);

            price = ModifyPriceFromType(request, price);



            return price;
        }

        public double ModifyPriceFromType(ExternalCommunicationRouteRequest request, double price)
        {
            switch (request.Type)
            {
                case "Standard":
                    break;
                case "RecordedDelevery":
                    // not suported
                    break;
                case "Weapons":
                    price = price * 2;
                    break;
                case "LiveAnimals":
                    // not suported
                    break;
                case "CautiousParcels":
                    price = price * 1.75;
                    break;
                case "RefrigeratedGoods":
                    price = price * 1.1;
                    break;
            }
            return price;

        }

        public double ModifyPriceFromdimentions(ExternalCommunicationRouteRequest request, double price)
        {
            if (request.Dimensions.All(s => s < 0.25))
            {
                price = price + 0;
            }
            else if (request.Dimensions.All(s => s < 0.40))
            {
                price = price + 8;

            }
            else if (request.Dimensions.All(s => s < 2))
            {
                price = price + 40;
            }
            return price;
        }
        public double ModifyPriceFromWeight(ExternalCommunicationRouteRequest request, double price)
        {
            if (request.Weight < 1000)
            {

                price = price + 0;
            }
            else if (request.Weight < 5000)
            {
                price = price + 20;
            }
            else
            {
                price = price + 40;
            }

            return price;
        }

    }
}
