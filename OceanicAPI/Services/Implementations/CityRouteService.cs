﻿using Common.Repositories.Implementations;
using Common.Repositories.Interfaces;
using OceanicAPI.Models;
using OceanicAPI.Models.External;
using OceanicAPI.Repositories.Interfaces;
using OceanicAPI.Services.Interfaces;

namespace OceanicAPI.Services.Implementations
{
    /// <inheritdoc/>
    public class CityRouteService : ICityRouteService
    {
        public CityRouteService(
            ICityRouteRepository cityRouteRepository)
        {
            CityRouteRepository = cityRouteRepository;
        }

        public ICityRouteRepository CityRouteRepository { get; }

        public IEnumerable<CityRoute> GetAllCityRoutes()
        {
            return CityRouteRepository.Read().ToList();
        }
    }
}
