using Microsoft.AspNetCore.Mvc;
using OceanicAPI.Models;
using OceanicAPI.Models.External;
using OceanicAPI.Services.Interfaces;

namespace OceanicAPI.Controllers
{
    [ApiController]
    [Route("/api/v0/[controller]")]
    public class ParcelController : ControllerBase
    {
        public ParcelController(
            IParcelService parcelService)
        {
            ParcelService = parcelService;
        }

        public IParcelService ParcelService { get; }

        [HttpGet]
        public IEnumerable<Parcel> GetAll()
        {
            return ParcelService.GetAll();
        }
    }
}