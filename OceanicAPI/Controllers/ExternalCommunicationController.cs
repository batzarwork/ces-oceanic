using Microsoft.AspNetCore.Mvc;
using OceanicAPI.Models.External;
using OceanicAPI.Services.Interfaces;

namespace OceanicAPI.Controllers
{
    [ApiController]
    [Route("/api/v0/")]
    public class ExternalCommunicationController : ControllerBase
    {
        public ExternalCommunicationController(
            IExternalCommunicationService externalCommunicationService)
        {
            ExternalCommunicationService = externalCommunicationService;
        }

        private IExternalCommunicationService ExternalCommunicationService { get; }

        [HttpPost]
        [Route("get-routes")]
        public IEnumerable<ExternalCommunicationRoute> Get(ExternalCommunicationRouteRequest request)
        {
            return ExternalCommunicationService.GetInternalRoutes(request);
        }

        [HttpPost]
        [Route("get-external-routes")]
        public async Task<IEnumerable<ExternalCommunicationRoute>> GetExternalRoute(ExternalCommunicationRouteRequest request, [FromQuery] string url)
        {
            return await ExternalCommunicationService.GetExternalRoutes(request, url);
        }
    }
}