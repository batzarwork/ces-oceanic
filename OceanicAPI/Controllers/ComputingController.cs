using Microsoft.AspNetCore.Mvc;
using OceanicAPI.Models;
using OceanicAPI.Models.External;
using OceanicAPI.Services.Interfaces;

namespace OceanicAPI.Controllers
{
    [ApiController]
    [Route("/api/v0/[controller]")]
    public class ComputingController : ControllerBase
    {
        public ComputingController(
            IComputingService computingService)
        {
            ComputingService = computingService;
        }

        public IComputingService ComputingService { get; }

        [HttpPost]
        public RouteInformation Computing(RequestParcel request)
        {
            return ComputingService.Computing(request);
        }
    }
}