using Microsoft.AspNetCore.Mvc;
using OceanicAPI.Models;
using OceanicAPI.Models.External;
using OceanicAPI.Services.Interfaces;

namespace OceanicAPI.Controllers
{
    [ApiController]
    [Route("/api/v0/[controller]")]
    public class DestinationController : ControllerBase
    {
        public DestinationController(
            IDestinationService destinationService)
        {
            DestinationService = destinationService;
        }

        public IDestinationService DestinationService { get; }

        [HttpGet]
        public IEnumerable<City> GetAll()
        {
            return DestinationService.GetAllCities();
        }
    }
}