using Microsoft.AspNetCore.Mvc;
using OceanicAPI.Models;
using OceanicAPI.Models.External;
using OceanicAPI.Services.Interfaces;

namespace OceanicAPI.Controllers
{
    [ApiController]
    [Route("/api/v0/[controller]")]
    public class BookingController : ControllerBase
    {
        public BookingController(
            IParcelService parcelService)
        {
            ParcelService = parcelService;
        }

        public IParcelService ParcelService { get; }

        [HttpPost]
        [Route("book")]
        public Parcel Book(Parcel parcel)
        {
            return ParcelService.Add(parcel);
        }
    }
}