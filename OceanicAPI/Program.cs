using Microsoft.EntityFrameworkCore;
using Oceanic.Context;

namespace OceanicAPI
{
    /// <summary>
    /// Program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Main class.
        /// </summary>
        /// <param name="args">Arguments.</param>
        /// <returns>>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static async Task Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Manually create startup and configure, to limit the amount of changes we have to make
            var startup = new Startup(builder.Configuration);

            startup.ConfigureServices(builder.Services);

            var app = builder.Build();

            startup.Configure(app, app.Environment);

            using (var scope = app.Services.CreateScope())
            {
                var databaseContext = scope.ServiceProvider.GetRequiredService<OceanicApiContext>();
                await databaseContext.Database.MigrateAsync();
            }

            await app.RunAsync();
        }
    }
}