﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OceanicAPI.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Parcels",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Weight = table.Column<int>(type: "int", nullable: false),
                    Dimensions = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    State = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EstimatedTimeArrival = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parcels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Routes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Routes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ParcelRoutes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CityOneId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CityTwoId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Price = table.Column<double>(type: "float", nullable: false),
                    EstimatedTimeArrival = table.Column<int>(type: "int", nullable: false),
                    ParcelId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TransportationType = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParcelRoutes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ParcelRoutes_Cities_CityOneId",
                        column: x => x.CityOneId,
                        principalTable: "Cities",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ParcelRoutes_Cities_CityTwoId",
                        column: x => x.CityTwoId,
                        principalTable: "Cities",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ParcelRoutes_Parcels_ParcelId",
                        column: x => x.ParcelId,
                        principalTable: "Parcels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CityRoutes",
                columns: table => new
                {
                    CityId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RouteId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CityRoutes", x => new { x.CityId, x.RouteId });
                    table.ForeignKey(
                        name: "FK_CityRoutes_Cities_CityId",
                        column: x => x.CityId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CityRoutes_Routes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("04b1949c-75a1-4b82-838b-0903a3814379"), "Marrakesh" },
                    { new Guid("178e3ba8-e3e3-401a-91a7-306a7ab3f637"), "Kabalo" },
                    { new Guid("17e7d18e-3a3b-44fb-987d-c265876e2623"), "Kap Guardafui" },
                    { new Guid("2a1d4ca8-830e-40ae-840c-bb20f894cbf7"), "Hvalbugten" },
                    { new Guid("2abfcbd3-f0ae-4f4c-bf2d-5be8119b6344"), "Cairo" },
                    { new Guid("325972ae-9157-4043-af46-0ae99301d613"), "Tripoli" },
                    { new Guid("325f4b31-37b8-4912-ba23-39fe59709487"), "Kapstaden" },
                    { new Guid("45dbf238-0976-47ee-a327-c5187c7e042b"), "Guldkysten" },
                    { new Guid("5ea45eff-496a-4ee9-9ce3-59bab2d814aa"), "Tanger" },
                    { new Guid("74f4dec7-535c-4ed6-87e3-a82b68335275"), "Sierra Leone" },
                    { new Guid("8a0ad3e0-ddf8-4d7d-8927-ec9ac44875aa"), "Dragebjerget" },
                    { new Guid("9d440382-d7ea-45a3-b45c-484569138eb4"), "Kap St. Marie" },
                    { new Guid("a0feddd9-69b2-4bff-b87e-8d98ab7ddf19"), "Victoria-søen" },
                    { new Guid("b6726a80-4576-48a6-95a7-bea17f28b160"), "Darfur" },
                    { new Guid("bc04251a-50b5-4947-b40d-2ee5fa246a42"), "Luanda" },
                    { new Guid("e993bd17-4c79-4af4-9e85-023b72f57a98"), "Suakin" },
                    { new Guid("fa8f76a2-33aa-46c0-9f88-df18664a410f"), "Amatave" },
                    { new Guid("ffb51563-61c4-499c-b93e-1b688145cb90"), "St. Helena" }
                });

            migrationBuilder.InsertData(
                table: "Parcels",
                columns: new[] { "Id", "Dimensions", "EstimatedTimeArrival", "Price", "StartDate", "State", "Type", "Weight" },
                values: new object[] { new Guid("7c7611fb-4c29-4244-a3b8-c8d585cb084d"), "[1.0,2.3,0.4]", new DateTime(2022, 8, 26, 12, 10, 10, 0, DateTimeKind.Unspecified), 100.0, new DateTime(2022, 8, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "complited", "LiveAnimal", 1000 });

            migrationBuilder.InsertData(
                table: "Routes",
                column: "Id",
                values: new object[]
                {
                    new Guid("085d026b-9194-4275-b6af-217e8d187d8d"),
                    new Guid("0b71b141-57c1-440e-a8ad-8323992c1ea8"),
                    new Guid("0bfbdfe7-a138-4d09-8235-e42ccaa77d11"),
                    new Guid("0cef40ee-f3ba-4496-b6de-b74ae4d962fb"),
                    new Guid("0df525c3-4043-4034-8ad9-044fcdfdc447"),
                    new Guid("10aa4d05-34e5-471f-8fb3-bc4ce9acbbdb"),
                    new Guid("10d0fcd7-10e8-4c06-a827-84b668e5f0e3"),
                    new Guid("152c42ca-45e8-4203-af0a-d5f2712b9637"),
                    new Guid("2a765283-71e3-40a5-b775-607b6a18d4b1"),
                    new Guid("6fd2f427-8521-43a2-8990-75caea1f28c0"),
                    new Guid("8548d2db-586a-4ed4-86ce-75d53f771fbd"),
                    new Guid("88a575b0-2f64-46c7-8f8f-b6ef8e2d506d"),
                    new Guid("a89e295e-1010-426c-aa9a-c08f8bf0c621"),
                    new Guid("b6be396d-3ceb-4657-9ed7-5da33e378d89"),
                    new Guid("c565afa1-4526-47fb-b475-3dc36cb2a32b"),
                    new Guid("d33d6643-f88b-49b1-90dd-ef42b81cc630"),
                    new Guid("d46db2fd-3ed5-4703-929a-86332506b775"),
                    new Guid("ec417fc0-135a-4dc4-a723-21545e00bba0"),
                    new Guid("fb2c8d13-91e3-44df-8aeb-8021695b3ee3"),
                    new Guid("fc9276f2-c221-4abd-b2e4-e8ea782e95d4")
                });

            migrationBuilder.InsertData(
                table: "CityRoutes",
                columns: new[] { "CityId", "RouteId", "Id" },
                values: new object[,]
                {
                    { new Guid("04b1949c-75a1-4b82-838b-0903a3814379"), new Guid("0b71b141-57c1-440e-a8ad-8323992c1ea8"), new Guid("281fa308-a3c7-45aa-927f-971a8e3bd217") },
                    { new Guid("04b1949c-75a1-4b82-838b-0903a3814379"), new Guid("fb2c8d13-91e3-44df-8aeb-8021695b3ee3"), new Guid("259fbc62-1fce-4309-90b3-f56910ed7eaa") },
                    { new Guid("04b1949c-75a1-4b82-838b-0903a3814379"), new Guid("fc9276f2-c221-4abd-b2e4-e8ea782e95d4"), new Guid("942bab04-00aa-4fc1-b1cb-d4630cfe50cd") },
                    { new Guid("178e3ba8-e3e3-401a-91a7-306a7ab3f637"), new Guid("8548d2db-586a-4ed4-86ce-75d53f771fbd"), new Guid("6303f5d1-665a-4f54-b3ac-2c872b5195d7") },
                    { new Guid("17e7d18e-3a3b-44fb-987d-c265876e2623"), new Guid("0df525c3-4043-4034-8ad9-044fcdfdc447"), new Guid("4306a2aa-269e-4398-a5d8-46dced945c97") },
                    { new Guid("17e7d18e-3a3b-44fb-987d-c265876e2623"), new Guid("c565afa1-4526-47fb-b475-3dc36cb2a32b"), new Guid("0661cc34-f8c1-4ebe-8fd4-14acaa434f35") },
                    { new Guid("2a1d4ca8-830e-40ae-840c-bb20f894cbf7"), new Guid("0bfbdfe7-a138-4d09-8235-e42ccaa77d11"), new Guid("166e9eb7-f141-4aee-8ffd-36e03dc0a54b") },
                    { new Guid("2a1d4ca8-830e-40ae-840c-bb20f894cbf7"), new Guid("d46db2fd-3ed5-4703-929a-86332506b775"), new Guid("d6fae90a-0d1b-4af1-b27b-c4dd1afadb79") },
                    { new Guid("2abfcbd3-f0ae-4f4c-bf2d-5be8119b6344"), new Guid("0cef40ee-f3ba-4496-b6de-b74ae4d962fb"), new Guid("4eea2cdb-60a5-48c9-86f2-2efd3a4e13e0") },
                    { new Guid("325972ae-9157-4043-af46-0ae99301d613"), new Guid("085d026b-9194-4275-b6af-217e8d187d8d"), new Guid("d615fe0c-0942-483a-ad5b-21942f0d9119") },
                    { new Guid("325972ae-9157-4043-af46-0ae99301d613"), new Guid("a89e295e-1010-426c-aa9a-c08f8bf0c621"), new Guid("cf58945b-95b6-4158-a16d-e75b7d938fe2") },
                    { new Guid("325972ae-9157-4043-af46-0ae99301d613"), new Guid("d33d6643-f88b-49b1-90dd-ef42b81cc630"), new Guid("040c3f0c-9abf-4ecf-803c-263acd91eae5") },
                    { new Guid("45dbf238-0976-47ee-a327-c5187c7e042b"), new Guid("0bfbdfe7-a138-4d09-8235-e42ccaa77d11"), new Guid("5dcce982-4c73-43ec-ad55-5cc4c91ff9ac") },
                    { new Guid("45dbf238-0976-47ee-a327-c5187c7e042b"), new Guid("152c42ca-45e8-4203-af0a-d5f2712b9637"), new Guid("05a4f6be-bbbd-4114-bd02-245226c1b358") },
                    { new Guid("45dbf238-0976-47ee-a327-c5187c7e042b"), new Guid("d33d6643-f88b-49b1-90dd-ef42b81cc630"), new Guid("650af950-ca04-4497-ad51-ab5ae406f203") },
                    { new Guid("45dbf238-0976-47ee-a327-c5187c7e042b"), new Guid("fb2c8d13-91e3-44df-8aeb-8021695b3ee3"), new Guid("f294c8fb-8f4d-462c-b735-d2f6ef25bdb2") },
                    { new Guid("5ea45eff-496a-4ee9-9ce3-59bab2d814aa"), new Guid("a89e295e-1010-426c-aa9a-c08f8bf0c621"), new Guid("05cade1b-475e-4598-8eda-f6687e0b147d") },
                    { new Guid("5ea45eff-496a-4ee9-9ce3-59bab2d814aa"), new Guid("fc9276f2-c221-4abd-b2e4-e8ea782e95d4"), new Guid("37f7e5e4-22d6-4e1b-b799-ccefb58e2991") },
                    { new Guid("74f4dec7-535c-4ed6-87e3-a82b68335275"), new Guid("0b71b141-57c1-440e-a8ad-8323992c1ea8"), new Guid("228f7560-fddb-4c84-b7da-e285f8faf364") },
                    { new Guid("74f4dec7-535c-4ed6-87e3-a82b68335275"), new Guid("ec417fc0-135a-4dc4-a723-21545e00bba0"), new Guid("b882dac1-d861-40e0-9884-67360c4fcd2f") },
                    { new Guid("8a0ad3e0-ddf8-4d7d-8927-ec9ac44875aa"), new Guid("88a575b0-2f64-46c7-8f8f-b6ef8e2d506d"), new Guid("e6f04df6-610b-43be-85da-e430a0089b16") },
                    { new Guid("a0feddd9-69b2-4bff-b87e-8d98ab7ddf19"), new Guid("2a765283-71e3-40a5-b775-607b6a18d4b1"), new Guid("1714fdce-f020-4e9d-91e4-07006be7428a") },
                    { new Guid("a0feddd9-69b2-4bff-b87e-8d98ab7ddf19"), new Guid("88a575b0-2f64-46c7-8f8f-b6ef8e2d506d"), new Guid("4dccab2e-bd7b-4d2a-9f98-62fe9e16336f") },
                    { new Guid("a0feddd9-69b2-4bff-b87e-8d98ab7ddf19"), new Guid("c565afa1-4526-47fb-b475-3dc36cb2a32b"), new Guid("5a38dde2-2d47-4484-a205-21a1aadfc328") },
                    { new Guid("b6726a80-4576-48a6-95a7-bea17f28b160"), new Guid("085d026b-9194-4275-b6af-217e8d187d8d"), new Guid("48834456-1cb4-4a97-9ec6-3576a4890c78") },
                    { new Guid("b6726a80-4576-48a6-95a7-bea17f28b160"), new Guid("6fd2f427-8521-43a2-8990-75caea1f28c0"), new Guid("14d54849-0164-460d-b1f5-ae5c82091f1e") },
                    { new Guid("b6726a80-4576-48a6-95a7-bea17f28b160"), new Guid("8548d2db-586a-4ed4-86ce-75d53f771fbd"), new Guid("a3d28cef-4e7f-48a9-9df3-ee0f62748d62") },
                    { new Guid("bc04251a-50b5-4947-b40d-2ee5fa246a42"), new Guid("152c42ca-45e8-4203-af0a-d5f2712b9637"), new Guid("3a370bf0-64a4-4420-a0ff-5f42f11671f3") },
                    { new Guid("bc04251a-50b5-4947-b40d-2ee5fa246a42"), new Guid("d46db2fd-3ed5-4703-929a-86332506b775"), new Guid("8be17572-c8d2-4ad3-8b1d-67e5f6e10dc5") },
                    { new Guid("e993bd17-4c79-4af4-9e85-023b72f57a98"), new Guid("0cef40ee-f3ba-4496-b6de-b74ae4d962fb"), new Guid("9a2e4d88-fe40-49b9-aa1a-288410077951") },
                    { new Guid("e993bd17-4c79-4af4-9e85-023b72f57a98"), new Guid("2a765283-71e3-40a5-b775-607b6a18d4b1"), new Guid("ceba553b-38fc-4715-9762-97213a7f03a3") },
                    { new Guid("e993bd17-4c79-4af4-9e85-023b72f57a98"), new Guid("6fd2f427-8521-43a2-8990-75caea1f28c0"), new Guid("8c2e143f-bee5-4285-80ce-a23744c3f7b3") },
                    { new Guid("fa8f76a2-33aa-46c0-9f88-df18664a410f"), new Guid("0df525c3-4043-4034-8ad9-044fcdfdc447"), new Guid("12e841b1-c9bb-492d-b3d1-4ee7a3d900fc") },
                    { new Guid("ffb51563-61c4-499c-b93e-1b688145cb90"), new Guid("ec417fc0-135a-4dc4-a723-21545e00bba0"), new Guid("cf739e9b-560a-466b-9d8f-6165866c44bb") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CityRoutes_RouteId",
                table: "CityRoutes",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_ParcelRoutes_CityOneId",
                table: "ParcelRoutes",
                column: "CityOneId");

            migrationBuilder.CreateIndex(
                name: "IX_ParcelRoutes_CityTwoId",
                table: "ParcelRoutes",
                column: "CityTwoId");

            migrationBuilder.CreateIndex(
                name: "IX_ParcelRoutes_ParcelId",
                table: "ParcelRoutes",
                column: "ParcelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CityRoutes");

            migrationBuilder.DropTable(
                name: "ParcelRoutes");

            migrationBuilder.DropTable(
                name: "Routes");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Parcels");
        }
    }
}
