﻿using Common.Repositories.Implementations;
using Microsoft.EntityFrameworkCore;
using Oceanic.Context;
using OceanicAPI.Models;
using OceanicAPI.Repositories.Interfaces;

namespace OceanicAPI.Repositories.Implementations
{
    public class CityRouteRepository : Repository<CityRoute>, ICityRouteRepository
    {
        public CityRouteRepository(OceanicApiContext oceanicApiContext)
            : base(oceanicApiContext)
        {
        }
    }
}
