﻿using Common.Repositories.Implementations;
using Microsoft.EntityFrameworkCore;
using Oceanic.Context;
using OceanicAPI.Models;
using OceanicAPI.Repositories.Interfaces;

namespace OceanicAPI.Repositories.Implementations
{
    public class DestinationRepository : Repository<City>, IDestinationRepository
    {
        public DestinationRepository(OceanicApiContext oceanicApiContext)
            : base(oceanicApiContext)
        {
        }
    }
}
