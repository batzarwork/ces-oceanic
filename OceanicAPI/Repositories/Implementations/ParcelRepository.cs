﻿using Oceanic.Context;
using OceanicAPI.Models;
using OceanicAPI.Repositories.Interfaces;

namespace OceanicAPI.Repositories.Implementations
{
    public class ParcelRepository : Repository<Parcel>, IParcelRepository
    {
        public ParcelRepository(OceanicApiContext oceanicApiContext)
            : base(oceanicApiContext)
        {
        }
    }
}
