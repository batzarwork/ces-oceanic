﻿using Oceanic.Context;
using OceanicAPI.Repositories.Interfaces;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Database.Repository
{
    [ExcludeFromCodeCoverage]
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly OceanicApiContext oceanicApiContext;

        public BaseRepository(OceanicApiContext oceanicApiContext)
        {
            this.oceanicApiContext = oceanicApiContext;
        }

        public T Create(T entity)
        {
            oceanicApiContext.Set<T>().Add(entity);
            oceanicApiContext.SaveChanges();

            return entity;
        }

        public IEnumerable<T> Create(IEnumerable<T> entity)
        {
            foreach (var a in entity)
            {
                oceanicApiContext.Set<T>().Add(a);
            }

            oceanicApiContext.SaveChanges();

            return entity;
        }

        public IEnumerable<T> Create(params T[] entity)
        {
            return Create(entity.AsEnumerable());
        }

        public IQueryable<T> Read()
        {
            return oceanicApiContext.Set<T>();
        }

        public T Update(T entity)
        {
            oceanicApiContext.Set<T>().Update(entity);
            oceanicApiContext.SaveChanges();

            return entity;
        }

        public IEnumerable<T> Update(IEnumerable<T> entity)
        {
            foreach (var a in entity)
            {
                oceanicApiContext.Set<T>().Update(a);
            }

            oceanicApiContext.SaveChanges();

            return entity;
        }

        public IEnumerable<T> Update(params T[] entity)
        {
            return Update(entity.AsEnumerable());
        }

        public T Delete(T entity)
        {
            oceanicApiContext.Set<T>().Remove(entity);
            oceanicApiContext.SaveChanges();

            return entity;
        }

        public IEnumerable<T> Delete(IEnumerable<T> entity)
        {
            foreach (var a in entity)
            {
                oceanicApiContext.Set<T>().Remove(a);
            }

            oceanicApiContext.SaveChanges();

            return entity;
        }

        public IEnumerable<T> Delete(params T[] entity)
        {
            return Delete(entity.AsEnumerable());
        }
    }
}
