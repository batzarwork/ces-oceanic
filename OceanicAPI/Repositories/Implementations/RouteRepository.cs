﻿using Oceanic.Context;
using OceanicAPI.Repositories.Interfaces;

namespace OceanicAPI.Repositories.Implementations
{
    public class RouteRepository : Repository<Models.Route>, IRouteRepository
    {
        public RouteRepository(OceanicApiContext oceanicApiContext)
            : base(oceanicApiContext)
        {
        }
    }
}
