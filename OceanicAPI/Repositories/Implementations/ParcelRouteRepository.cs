﻿using Common.Repositories.Implementations;
using Microsoft.EntityFrameworkCore;
using Oceanic.Context;
using OceanicAPI.Models;
using OceanicAPI.Repositories.Interfaces;

namespace OceanicAPI.Repositories.Implementations
{
    public class ParcelRouteRepository : Repository<ParcelRoute>, IParcelRouteRepository
    {
        public ParcelRouteRepository(OceanicApiContext oceanicApiContext)
            : base(oceanicApiContext)
        {
        }
    }
}
