﻿using OceanicAPI.Models;

namespace OceanicAPI.Repositories.Interfaces
{
    public interface IParcelRepository : IRepository<Parcel>
    {
    }
}