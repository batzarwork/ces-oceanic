﻿namespace OceanicAPI.Repositories.Interfaces
{
    public interface IRouteRepository : IRepository<Models.Route>
    {
    }
}