﻿using OceanicAPI.Models;
using OceanicAPI.Repositories.Interfaces;

namespace OceanicAPI.Repositories.Interfaces
{
    public interface IParcelRouteRepository : IRepository<ParcelRoute>
    {
    }
}