﻿using System.Collections.Generic;
using System.Linq;

namespace OceanicAPI.Repositories.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T Create(T entity);
        IEnumerable<T> Create(IEnumerable<T> entity);
        IEnumerable<T> Create(params T[] entity);
        IQueryable<T> Read();
        T Update(T entity);
        IEnumerable<T> Update(IEnumerable<T> entity);
        IEnumerable<T> Update(params T[] entity);
        T Delete(T entity);
        IEnumerable<T> Delete(IEnumerable<T> entity);
        IEnumerable<T> Delete(params T[] entity);
    }
}