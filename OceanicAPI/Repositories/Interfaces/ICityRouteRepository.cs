﻿using OceanicAPI.Models;
using OceanicAPI.Repositories.Interfaces;

namespace OceanicAPI.Repositories.Interfaces
{
    public interface ICityRouteRepository : IRepository<CityRoute>
    {
    }
}