﻿using OceanicAPI.Models;

namespace OceanicAPI.Repositories.Interfaces
{
    public interface IDestinationRepository : IRepository<City>
    {
    }
}