﻿using System.Linq.Expressions;

namespace Common.Repositories.Interfaces
{
    /// <summary>
    /// Generic repository.
    /// </summary>
    /// <typeparam name="TEntity">Entity model.</typeparam>
    public interface IGenericRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Delete entity.
        /// </summary>
        /// <param name="id">Id.</param>
        void Delete(object id);

        /// <summary>
        /// Delete entity.
        /// </summary>
        /// <param name="entityToDelete">Entity to delete.</param>
        void Delete(TEntity entityToDelete);

        /// <summary>
        /// Get by expresions.
        /// </summary>
        /// <param name="filter">Filter.</param>
        /// <param name="orderBy">Order by.</param>
        /// <param name="includeProperties">Included properties.</param>
        /// <returns>IEnumerable of TEntity.</returns>
        Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>>? filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null, string includeProperties = "");

        /// <summary>
        /// Get by id.
        /// </summary>
        /// <param name="id">Get by Id.</param>
        /// <returns>Instance of entity model.</returns>
        Task<TEntity?> GetByID(object id);

        /// <summary>
        /// Insert entity.
        /// </summary>
        /// <param name="entity">Entity.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task Insert(TEntity entity);

        /// <summary>
        /// Save changes.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task SaveChanges();

        /// <summary>
        /// Update entity.
        /// </summary>
        /// <param name="entityToUpdate">Entity to update.</param>
        void Update(TEntity entityToUpdate);
    }
}