using Common.Populators.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Common.ServiceConfiguration
{
    /// <summary>
    /// Configure services for populators.
    /// </summary>
    public static class PopulatorConfiguration
    {
        /// <summary>
        /// Add Populators.
        /// </summary>
        /// <param name="serviceCollection">Service collection.</param>
        /// <returns>The <see cref="IServiceCollection"/>Provided as input to allow for chaining.</returns>
        public static IServiceCollection AddPopulators(this IServiceCollection serviceCollection)
        {
            //serviceCollection.AddScoped<IPopulator<DripNotification, Notification>, NotificationPopulator>();
            return serviceCollection;
        }
    }
}
