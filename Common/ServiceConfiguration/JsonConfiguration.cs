using Common.Options;
using Microsoft.Extensions.DependencyInjection;

namespace Common.ServiceConfiguration
{
    /// <summary>
    /// Configration of Json serialization.
    /// </summary>
    public static class JsonConfiguration
    {
        /// <summary>
        /// Configure json serialization for an <see cref="IMvcBuilder"/>.
        /// </summary>
        /// <param name="mvcBuilder">Instance of <see cref="IMvcBuilder"/> to configure.</param>
        public static void AddJsonConfiguration(this IMvcBuilder mvcBuilder)
        {
            mvcBuilder.AddJsonOptions(options =>
             {
                 options.JsonSerializerOptions.PropertyNameCaseInsensitive = JsonOptions.PropertyNameCaseInsensitive;
                 options.JsonSerializerOptions.PropertyNamingPolicy = JsonOptions.JsonNamingPolicy;
             });
        }
    }
}
