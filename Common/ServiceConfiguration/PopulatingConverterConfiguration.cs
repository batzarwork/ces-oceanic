using Common.Populators.Implementations;
using Common.Populators.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Common.ServiceConfiguration
{
    /// <summary>
    /// Configure services for populating converters.
    /// </summary>
    public static class PopulatingConverterConfiguration
    {
        /// <summary>
        /// Add Populating Converters.
        /// </summary>
        /// <param name="serviceCollection">Service collection.</param>
        /// <returns>The <see cref="IServiceCollection"/>Provided as input to allow for chaining.</returns>
        public static IServiceCollection AddPopulatingConverters(this IServiceCollection serviceCollection)
        {
            //serviceCollection.AddScoped<IPopulatingConverter<DripNotification, Notification>, PopulatingConverter<DripNotification, Notification>>();
            return serviceCollection;
        }
    }
}
