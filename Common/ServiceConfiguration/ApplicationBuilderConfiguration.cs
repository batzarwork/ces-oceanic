using Common.ErrorHandling;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Common.ServiceConfiguration
{
    /// <summary>
    /// Configuration of ApplicationBuilder for common
    /// settings for the http pipeline.
    /// </summary>
    public static class ApplicationBuilderConfiguration
    {
        /// <summary>
        /// Common configuration of the http pipeline.
        /// </summary>
        /// <param name="app">The application builder for the application.</param>
        /// <param name="openApiVersion">Version of OpenAPI definition to use if Swagger UI is enabled.</param>
        /// <param name="env">The applications environments.</param>
        /// <param name="forceSwagger">If set to true, includes uses swagger regardless of environment.
        /// If set to false, swagger is only available if run in a development environment.</param>
        /// <returns>The application builder provided as input.</returns>
        public static IApplicationBuilder ConfigureBuilder(this IApplicationBuilder app, string openApiVersion, IWebHostEnvironment env, bool forceSwagger = false)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //if (env.IsDevelopment() || env.IsStaging() || forceSwagger)
            //{
            //    app.UseSwagger(options =>
            //    {
            //        options.PreSerializeFilters.Add((swagger, httpReq) =>
            //        {
            //            if (httpReq.Headers.ContainsKey("X-Forwarded-Host") && httpReq.Headers.ContainsKey("X-Forwarded-Prefix"))
            //            {
            //                var basePath = $"{httpReq.Headers["X-Forwarded-Prefix"]}";
            //                var serverUrl = $"{httpReq.Headers["X-Forwarded-Proto"]}://{httpReq.Headers["X-Forwarded-Host"]}/{basePath}";
            //                swagger.Servers = new List<OpenApiServer> { new OpenApiServer { Url = serverUrl } };
            //            }
            //        });
            //    }).UseSwaggerUI(c => c.SwaggerEndpoint($"{openApiVersion}/swagger.json", "FlexDanmark API"));
            //}

            app.UseSwagger();
            app.UseSwaggerUI();
            app.UseRouting();

            app.UseExceptionHandler(new ExceptionHandlerOptions
            {
                ExceptionHandler = new ExceptionHandler(env).Invoke,
            });

            app.UseCors();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            return app;
        }
    }
}
