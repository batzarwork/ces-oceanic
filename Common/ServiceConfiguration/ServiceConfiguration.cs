﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Common.ServiceConfiguration
{
    /// <summary>
    /// Configure common services.
    /// </summary>
    public static class ServiceConfiguration
    {
        /// <summary>
        /// Configure converters and populators services that are used across
        /// all APIs.
        /// </summary>
        /// <param name="serviceCollection">Service collection.</param>
        /// <returns>The <see cref="IServiceCollection"/> provided as input to allow for chaining.</returns>
        public static IServiceCollection AddConvertersAndPopulators(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddPopulatingConverters();
            serviceCollection.AddPopulators();
            return serviceCollection;
        }

        /// <summary>
        /// Performs all common configuration for services in the API layer.
        ///
        /// </summary>
        /// <param name="serviceCollection">The <see cref="IServiceCollection"/> to set up.</param>
        /// <returns>The <see cref="IServiceCollection"/> provided as input to allow for chaning.</returns>
        public static IServiceCollection ConfigureServices(
            this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSwaggerGen();
            serviceCollection.AddControllers().AddJsonConfiguration();

            serviceCollection.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.WithOrigins("*").AllowAnyHeader().AllowAnyMethod();
                    });
            });
            return serviceCollection;
        }
    }
}
