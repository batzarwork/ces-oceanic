using Common.Populators.Interfaces;

namespace Common.Populators.Implementations
{
    /// <inheritdoc/>
    public class PopulatingConverter<TInput, TOutput> : IPopulatingConverter<TInput, TOutput>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PopulatingConverter{TInput, TOutput}"/> class.
        /// </summary>
        /// <param name="populators">List of <see cref="IPopulator{TInput, TOutput}"/> to use.</param>
        public PopulatingConverter(IEnumerable<IPopulator<TInput, TOutput>> populators)
        {
            Populators = populators?.ToList() ?? new List<IPopulator<TInput, TOutput>>();
        }

        /// <inheritdoc/>
        public List<IPopulator<TInput, TOutput>> Populators { get; }

        /// <inheritdoc/>
        public TOutput Convert(TInput source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(typeof(TInput).Name);
            }

            var target = CreateFromClass(source);

            foreach (IPopulator<TInput, TOutput> populator in Populators)
            {
                try
                {
                    populator.Populate(source, target);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            return target;
        }

        /// <inheritdoc/>
        public IEnumerable<TOutput> ConvertAll(IEnumerable<TInput> objectsToConvert)
        {
            return objectsToConvert.Select(Convert);
        }

        /// <summary>
        /// Create an Instance of target class.
        /// </summary>
        /// <param name="source">Source to be converted.</param>
        /// <returns>Instance of target class.</returns>
        protected virtual TOutput CreateFromClass(TInput source)
        {
            return Activator.CreateInstance<TOutput>();
        }
    }
}
