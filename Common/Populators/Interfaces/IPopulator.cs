namespace Common.Populators.Interfaces
{
    /// <summary>
    /// Interface for converting/populating data.
    /// </summary>
    /// <typeparam name="TInput">Source Type.</typeparam>
    /// <typeparam name="TOutput">Target Type.</typeparam>
    public interface IPopulator<in TInput, in TOutput>
    {
        /// <summary>
        /// Populate data from source to target object.
        /// </summary>
        /// <param name="source">Source object.</param>
        /// <param name="target">Target object.</param>
        void Populate(TInput source, TOutput target);
    }
}
