﻿namespace Common.Populators.Interfaces
{
    /// <summary>
    /// Convert and Populate Data from TInput object to TOutput object.
    /// Pattern: https://keepcodeclean.com/converters-and-populators-design-patterns/.
    /// </summary>
    /// <typeparam name="TInput">Type of source object.</typeparam>
    /// <typeparam name="TOutput">Type of target object.</typeparam>
    public interface IPopulatingConverter<TInput, TOutput>
    {
        /// <summary>
        /// List of Populators to generate data accordingly.
        /// </summary>
        List<IPopulator<TInput, TOutput>> Populators { get; }

        /// <summary>
        /// Convert data with Populator and return the target object.
        /// </summary>
        /// <param name="source">Source object.</param>
        /// <returns>Target object after populating data.</returns>
        TOutput Convert(TInput source);

        /// <summary>
        /// Convert every source objects to target objects and populate data.
        /// </summary>
        /// <param name="objectsToConvert">List of source objects.</param>
        /// <returns>List of target objects.</returns>
        IEnumerable<TOutput> ConvertAll(IEnumerable<TInput> objectsToConvert);
    }
}
