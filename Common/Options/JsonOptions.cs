using System.Text.Json;

namespace Common.Options
{
    /// <summary>
    /// Shared JsonSettings.
    /// </summary>
    public class JsonOptions
    {
        /// <summary>
        /// Case sensitivity for deserialisation.
        /// </summary>
        public static readonly bool PropertyNameCaseInsensitive = true;

        /// <summary>
        /// Case for serialisation.
        /// </summary>
        public static readonly JsonNamingPolicy JsonNamingPolicy = JsonNamingPolicy.CamelCase;

        /// <summary>
        /// Shared <see cref="JsonSerializerOptions"/>.
        /// </summary>
        public static readonly JsonSerializerOptions JsonSerializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = PropertyNameCaseInsensitive,
            PropertyNamingPolicy = JsonNamingPolicy,
            DictionaryKeyPolicy = JsonNamingPolicy,
        };

        private JsonOptions()
        {
        }
    }
}
