using System.Text.Json.Serialization;

namespace Common.ErrorHandling
{
    /// <summary>
    /// Error model for APIs.
    /// </summary>
    public class ApiError
    {
        /// <summary>
        /// Error message.
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string? Message { get; set; }

        /// <summary>
        /// Additional detail about the error.
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string? Detail { get; set; }

        /// <summary>
        /// String representation of an inner exception.
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string? InnerException { get; set; }
    }
}
