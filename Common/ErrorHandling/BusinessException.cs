using System;
using System.Net;
using System.Runtime.Serialization;

namespace Common.ErrorHandling
{
    /// <summary>
    /// BusinessException and its subclasses represents business logic
    /// errors, such as invalid input.
    ///
    /// Developers should be aware, that error messages in BusinessExeptions
    /// are returned to the user, so no confidential information should be
    /// included.
    /// </summary>
    [Serializable]
    public class BusinessException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessException"/> class.
        /// </summary>
        public BusinessException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="statusCode">Optional statuscode if something other than the
        /// default should be returned.</param>
        public BusinessException(string? message, HttpStatusCode? statusCode = null)
            : base(message)
        {
            StatusCode = statusCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="innerException">Inner <see cref="Exception"/>
        /// that caused this Exception to Occr.</param>
        /// <param name="statusCode">Optional statuscode if something other than the
        /// default should be returned.</param>
        public BusinessException(string message, Exception innerException, HttpStatusCode? statusCode = null)
            : base(message, innerException)
        {
            StatusCode = statusCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessException"/> class.
        ///
        /// This constructor is only used for deserialization.
        /// </summary>
        /// <param name="info"><see cref="SerializationInfo"/>.</param>
        /// <param name="context"><see cref="StreamingContext"/>.</param>
        protected BusinessException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Gets or sets the <see cref="HttpStatusCode"/>
        /// of the exception.
        /// </summary>
        public HttpStatusCode? StatusCode { get; set; }
    }
}
