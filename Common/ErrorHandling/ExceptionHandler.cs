using System.Net;
using System.Text.Json;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Common.ErrorHandling
{
    /// <summary>
    /// Handling of Exceptions in the asp.net core pipeline.
    ///
    /// Generic handling, that ensures that only Business logic exception
    /// details are returned to the callee, while other errors are returned as
    /// internal service errors.
    /// </summary>
    public class ExceptionHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandler"/> class.
        /// </summary>
        /// <param name="hostingEnvironment">The current environment.</param>
        public ExceptionHandler(IWebHostEnvironment hostingEnvironment)
        {
            HostingEnvironment = hostingEnvironment;
        }

        private IWebHostEnvironment HostingEnvironment { get; }

        /// <summary>
        /// Handles exceptions in the pipeline.
        /// </summary>
        /// <param name="httpContext">Current call context.</param>
        /// <returns>Task representing the error output.</returns>
        public async Task Invoke(HttpContext httpContext)
        {
            var exception = httpContext.Features.Get<IExceptionHandlerFeature>()?.Error;
            if (exception == null)
            {
                return;
            }

            // There is an exception
            var error = new ApiError
            {
                Message = exception.Message,
                Detail = exception.StackTrace,
                InnerException = exception.InnerException != null
                    ? exception.InnerException.Message
                    : string.Empty,
            };

            var statusCode = (int)HttpStatusCode.InternalServerError;
            if (exception is BusinessException businessException)
            {
                statusCode =
                    businessException.StatusCode != null
                        ? (int)businessException.StatusCode
                        : (int)HttpStatusCode.InternalServerError;
            }

            httpContext.Response.StatusCode = statusCode;
            httpContext.Response.ContentType = "application/json";

            await using var writer = new StreamWriter(httpContext.Response.Body);
            var jsonError = JsonSerializer.Serialize(error);
            await writer.WriteAsync(jsonError);
        }
    }
}