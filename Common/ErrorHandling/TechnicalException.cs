using System;
using System.Runtime.Serialization;

namespace FlexDanmark.Nop.Core.Api.Common.ErrorHandling
{
    /// <summary>
    /// System exception and subclasses represent
    /// technical errors that happen in the system.
    /// </summary>
    [Serializable]
    public class TechnicalException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TechnicalException"/> class.
        /// </summary>
        public TechnicalException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TechnicalException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        public TechnicalException(string? message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TechnicalException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="innerException">Inner <see cref="Exception"/>
        /// that caused this Exception to Occr.</param>
        public TechnicalException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TechnicalException"/> class.
        ///
        /// This constructor is only used for deserialization.
        /// </summary>
        /// <param name="info"><see cref="SerializationInfo"/>.</param>
        /// <param name="context"><see cref="StreamingContext"/>.</param>
        protected TechnicalException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
